/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.acesinc.test.web.repository;

import java.util.List;
import net.acesinc.test.model.User;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author andrewserff
 */
public interface UserRepository extends PagingAndSortingRepository<User, String> {
    public User findByEmailAndPassword(String email, String password);
    public User findByEmail(String email);
//    @Query(value = "{'email': {$regex : ?0, $options: 'i'}}")
    public List<User> findByEmailRegex(String query);
}
