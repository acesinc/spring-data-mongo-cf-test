/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.acesinc.test.web.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import net.acesinc.test.model.Result;
import net.acesinc.test.model.User;
import net.acesinc.test.web.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author andrewserff
 */
@Controller
public class UserController {

    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserRepository userRepo;
    @Autowired
    private PasswordEncoder encoder;

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String getRegistrationPage(ModelMap model) {
        return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerUser(@RequestParam("email") String email, @RequestParam("password") String password, ModelMap model) {

        User test = userRepo.findByEmail(email);
        if (test != null) {
            log.warn("User tried to register with email that already has an account");
            model.addAttribute("error", true);
            if (test.isEnabled()) {
                model.addAttribute("errMessage", "Email Address is already registered.  Did you forget your password?");
            } else {
                model.addAttribute("errMessage", "Email Address is already registered but has not been verified");
            }
            return "register";
        } else {

            User u = new User();

            u.setEmail(email);
            u.setPassword(encodePassword(password));

            log.info("Registering new user [ " + u.getUsername() + ", " + password + " ]");
            userRepo.save(u);
            log.info("New User [ " + u.getUsername() + " ] saved with id [ " + u.getId() + "]");

            model.addAttribute("message", "Congrats on being a registered user!");

            return "login";
        }
    }

    private String encodePassword(String password) {
        //encode the password
        String encodedPassword = encoder.encode(password);
        log.debug("Encoded password [ " + password + " ] to [ " + encodedPassword + " ]");
        return encodedPassword;
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public @ResponseBody
    Result getUsers(Principal user, @RequestParam(value = "email", required = false) String email) {
        User u = userRepo.findByEmail(user.getName());
        if (u != null) {
            List<User> users = new ArrayList<>();
            if (email != null && !email.isEmpty()) {
                users = userRepo.findByEmailRegex(email);
                log.debug("Found [ " + users.size() + " ] that match query [ " + email + " ]");
            } else {
                return Result.error(null, "Invalid query specified. You must specify [ firstName | lastName | name | company ]");
            }
            
            return Result.ok(users);
        } else {
            return Result.error(null, "Unable to verify User Profile");
        }
    }
}
