<%-- 
    Document   : index
    Created on : Aug 12, 2014, 2:19:19 PM
    Author     : andrewserff
--%>

<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html class="fixed">
    <head>

        <!-- Basic -->
        <meta charset="UTF-8">

        <title>Login</title>
        <meta name="keywords" content="ACES ATS Applicant Tracking Resume CV" />
        <meta name="description" content="ACES Applicant Tracking System">
        <meta name="author" content="acesinc.net">

        <sec:csrfMetaTags />

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        <!-- Web Fonts  -->
        <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

        <!-- Vendor CSS -->
        <link rel="stylesheet" href="/assets/vendor/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="/assets/vendor/font-awesome/css/font-awesome.css" />
        <link rel="stylesheet" href="/assets/vendor/magnific-popup/magnific-popup.css" />
        <link rel="stylesheet" href="/assets/vendor/bootstrap-datepicker/css/datepicker3.css" />
        <link rel="stylesheet" href="/assets/vendor/pnotify/pnotify.custom.css" />

        <!-- Specific Page Vendor CSS -->

        <!-- Theme CSS -->
        <link rel="stylesheet" href="/assets/stylesheets/theme.css" />

        <!-- Skin CSS -->
        <link rel="stylesheet" href="/assets/stylesheets/skins/default.css" />

        <!-- Theme Custom CSS -->
        <link rel="stylesheet" href="/assets/stylesheets/theme-custom.css">

        <!-- Head Libs -->
        <script src="/assets/vendor/modernizr/modernizr.js"></script>

    </head>
    <body>


        <!-- start: page -->
        <section class="body-sign">
            <div class="center-sign">
                <a href="/" class="logo pull-left">
                    <img src="/assets/images/logo.png" height="54" alt="ATS Logo" />
                </a>

                <div class="panel panel-sign">
                    <div class="panel-title-sign mt-xl text-right">
                        <h2 class="title text-uppercase text-bold m-none"><i class="fa fa-user mr-xs"></i> Sign Up</h2>
                    </div>
                    <div class="panel-body">
                        <form id="register-form" action="/register" method="post">
                            <c:if test="${invite != null}"><input type="hidden" name="invite" value="${invite.id}"/></c:if>
                            <c:if test="${(param.error != null && SPRING_SECURITY_LAST_EXCEPTION.message != null) || errMessage != null}">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group mb-lg">
                                            <div class="alert alert-danger">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                <strong>Oh snap!</strong> <c:choose>
                                                    <c:when test="${SPRING_SECURITY_LAST_EXCEPTION.message != null}">${SPRING_SECURITY_LAST_EXCEPTION.message}</c:when>
                                                    <c:when test="${errMessage != null}">${errMessage}</c:when>
                                                    <c:otherwise>An error occurred</c:otherwise>
                                                </c:choose>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:if>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel panel-transparent">
                                        <div class="panel-heading">
                                            <span class="panel-title" >Account Info</span>
                                        </div>
                                        <div class="">
                                            
                                            <div class="form-group mb-lg">
                                                <label>E-mail Address</label>
                                                <div class="input-group input-group-icon">
                                                    <input name="email" type="email" class="form-control input-lg" placeholder="ex: frodo.baggins@theshire.net" <c:if test="${email != null}">value='${email}' disabled</c:if>/>
                                                        <span class="input-group-addon">
                                                            <span class="icon icon-lg">
                                                                <i class="fa fa-envelope"></i>
                                                            </span>
                                                        </span>
                                                    </div>
                                                    <!--<input name="email" type="email" class="form-control input-lg" placeholder="ex: frodo.baggins@theshire.net"/>-->
                                                </div>

                                                <div class="form-group mb-none">
                                                    <div class="row">
                                                        <div class="col-sm-6 mb-lg">
                                                            <label>Password</label>
                                                            <div class="input-group input-group-icon">
                                                                <input name="password" type="password" class="form-control input-lg"/>
                                                                <span class="input-group-addon">
                                                                    <span class="icon icon-lg">
                                                                        <i class="fa fa-lock"></i>
                                                                    </span>
                                                                </span>
                                                            </div>
                                                            <!--<input name="password" type="password" class="form-control input-lg" />-->
                                                        </div>
                                                        <div class="col-sm-6 mb-lg">
                                                            <label>Password Confirmation</label>
                                                            <div class="input-group input-group-icon">
                                                                <input name="password_confirm" type="password" class="form-control input-lg"/>
                                                                <span class="input-group-addon">
                                                                    <span class="icon icon-lg">
                                                                        <i class="fa fa-lock"></i>
                                                                    </span>
                                                                </span>
                                                            </div>
                                                            <!--<input name="password_confirm" type="password" class="form-control input-lg" />-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="checkbox-custom checkbox-default">
                                            <input id="AgreeTerms" name="agreeterms" type="checkbox"/>
                                            <label for="AgreeTerms">I agree with <a href="#">terms of use</a></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 text-right">
                                        <button type="submit" class="btn btn-primary hidden-xs">Sign Up</button>
                                        <button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">Sign Up</button>
                                    </div>
                                </div>

                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        </form>
                    </div>
                    <div class="panel-footer">
                        <p class="text-center">Already have an account yet? <a href="/login">Sign In!</a>
                    </div>
                </div>

                <p class="text-center text-muted mt-md mb-md">&copy; Copyright 2014. All Rights Reserved.</p>
            </div>
        </section>
        <!-- end: page -->

        <!-- Vendor -->
        <script src="/assets/vendor/jquery/jquery.js"></script>
        <script src="/assets/vendor/bootstrap/js/bootstrap.js"></script>
        <!--Have to put this here to make sure it overrides jquery-ui's datepicker-->
        <script src="/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>                
        <!-- Theme Base, Components and Settings -->
        <script src="/assets/javascripts/theme.js"></script>

        <!-- Theme Custom -->
        <script src="/assets/javascripts/theme.custom.js"></script>

        <!-- Theme Initialization Files -->
        <script src="/assets/javascripts/theme.init.js"></script>



        <script>
            $('#register-form').submit(function () {
                $("#register-form :disabled").removeAttr('disabled');
            });
        </script>
    </body>
</html>