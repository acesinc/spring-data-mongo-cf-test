<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html class="boxed">
    <head>

        <!-- Basic -->
        <meta charset="UTF-8">

        <title>${pageName} | ACES ATS</title>
        <meta name="keywords" content="ACES ATS Applicant Tracking Resume CV" />
        <meta name="description" content="ACES Applicant Tracking System">
        <meta name="author" content="acesinc.net">

        <sec:csrfMetaTags />

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        <!-- Web Fonts  -->
        <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

        <!-- Vendor CSS -->
        <link rel="stylesheet" href="/assets/vendor/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="/assets/vendor/font-awesome/css/font-awesome.css" />
        <link rel="stylesheet" href="/assets/vendor/magnific-popup/magnific-popup.css" />
        <link rel="stylesheet" href="/assets/vendor/bootstrap-datepicker/css/datepicker3.css" />
        <link rel="stylesheet" href="/assets/vendor/pnotify/pnotify.custom.css" />

        <!-- Specific Page Vendor CSS -->

    </head>
    <body style="background: white;">

        <header style="padding-top: 20px; margin-right: 20px;" >
            <div class="header-right">
                <a href="/login" class="btn btn-default">Login</a>
            </div>
        </header>
        <section class="body" >

            <div class="inner-wrapper">
                <section role="main" class="content-body" style="background-color: inherit;">
                    <!-- start: page -->
                    <h1>Hi</h1>
                    <!-- end: page -->
            </div>
        </div>
    </div>
    <!-- Vendor -->
    <script src="/assets/vendor/jquery/jquery.js"></script>
    <script src="/assets/vendor/bootstrap/js/bootstrap.js"></script>
    <!--Have to put this here to make sure it overrides jquery-ui's datepicker-->
    <script src="/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>                
    <!-- Theme Base, Components and Settings -->
    <script src="/assets/javascripts/theme.js"></script>

    <!-- Theme Custom -->
    <script src="/assets/javascripts/theme.custom.js"></script>

    <!-- Theme Initialization Files -->
    <script src="/assets/javascripts/theme.init.js"></script>

</body>
</html>