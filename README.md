# README #

A test web application that uses spring-data-mongo to store data in MongoDB and shows a timeout problem when running on Pivotal's Cloudfoundry hosting.  

To run this locally, you can do the following:
    mvn clean package tomcat7:run-war

Then access the webapp at http://localhost:8080
